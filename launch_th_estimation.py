import argparse
from src.feret_dataset import compute_th


def main():

    parser=argparse.ArgumentParser()

    parser.add_argument(
        "path_data",
        help="path to features file"
    )

    parser.add_argument(
        "path_label",
        help="path to labels file"
    )

    parser.add_argument(
        "path_mahalanobis",
        help="path to mahalanobis matrix"
    )



    args=parser.parse_args()


    compute_th(args.path_data, args.paths_label, args.path_mahalanobis)


if __name__ == "__main__":
    main()