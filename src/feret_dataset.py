
import os
from scipy.misc import imread
from src.utilities import extract_faces
from src.vgg import extract_features_no_pca, load_vgg
import numpy as np
from scipy.spatial.distance import pdist



def generate_dataset(path, target_dir):

    features=[]
    labels=[]

    vgg=load_vgg()
    count=0
    dir_list=os.listdir(path)
    for dir in dir_list:
        label=int(dir)
        path_dir= os.path.join(path, dir)
        for img in os.listdir(path_dir):
            img_path=os.path.join(path_dir, img)
            image=imread(img_path,  mode="RGB")
            b_boxes= extract_faces(image)
            if(len(b_boxes)):
                box=b_boxes[0]
                face = image[box[1]:box[1] + box[3], box[0]:box[0] + box[2]]
                feature = extract_features_no_pca(face, vgg, to_RGB=False)
                features.append(feature)
                labels.append(label)

        print(count)
        count+=1
    X = np.stack(features, axis=0)
    Y = np.stack(labels, axis=0)

    np.save(os.path.join(target_dir, "X_feret.npy"), X)

    np.save(os.path.join(target_dir, "Y_feret.npy"), Y)

def compute_th(X_path,Y_path, path_mahala):

    M = np.load(path_mahala)
    features = np.load(X_path)
    labels = np.load(Y_path)

    classes = np.unique(labels)

    min_distances = []

    for label in classes:
        min_dist = np.inf
        label_features = features[np.where(labels == label)]

        for other_feature in features[np.where(labels != label)]:
            for label_feature in label_features:
                dist = pdist(np.stack((label_feature, other_feature)), 'mahalanobis', VI=M)
                if dist < min_dist:
                    min_dist = dist
        min_distances.append(min_dist)
        print(str(min_dist)+"\n")

    print("median: "+ str(np.median(min_distances)))
    print("\nmean: "+ str(np.mean(min_distances)))
    #np.save("/home/leonardo/Desktop/th.txt", np.median(min_distances ))


if __name__=="__main__":
    # path="/home/leonardo/Desktop/dataset/colorferet/colorferet/mixed_dataset"
    # target_dir="/home/leonardo/Desktop/dataset/feret_extracted"
    # generate_dataset(path, target_dir)
    X_path = "/media/leonardo/82033660-3442-4626-a6ab-b2e6d2d8a4bf/dataset/feret_extracted/X_feret.npy"
    Y_path = "/media/leonardo/82033660-3442-4626-a6ab-b2e6d2d8a4bf/dataset/feret_extracted/Y_feret.npy"
    path_mahala = "/media/leonardo/82033660-3442-4626-a6ab-b2e6d2d8a4bf/dataset/feat_ds/M_feret_300.npy"
    compute_th(X_path,Y_path,path_mahala)