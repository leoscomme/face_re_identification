from metric_learn import SDML
from metric_learn import SDML_Supervised,RCA_Supervised,LFDA
import numpy as np
import os




def learn_metric(path_data,path_label, path_dest):
    X=np.load(path_data)
    Y=np.load(path_label)
    Y=np.uint32(Y)
    rca= RCA_Supervised(num_chunks=1000, chunk_size=3, pca_comps=300)
    rca.fit(X,Y)
    M=rca.metric()
    np.save(path_dest, M)

if __name__=="__main__":

    path_data= "/home/leonardo/Desktop/dataset/feret_extracted/X_feret.npy"
    path_label= "/home/leonardo/Desktop/dataset/feret_extracted/Y_feret.npy"
    path_dest="/home/leonardo/Desktop/dataset/feat_ds/M_feret_300.npy"
    learn_metric(path_data, path_label, path_dest)
