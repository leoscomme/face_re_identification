import cv2
#from vgg_alt import * #TODO quanto è utile
from src.Subject import Subject
import numpy as np
import os

from src.vgg import extract_features_no_pca



def load_subjects():
    return []


#ritorna la lista dei bounding box ciascuno contenente una faccia presente nel frame nel formato (x,y,w,h) con x;y coordinate ancolo sx w la larghezza e h altezza del bounding box
def extract_faces(img):
    #face_cascade = cv2.CascadeClassifier('haarcascade_xml/haarcascade_frontalface_alt.xml') #
    face_cascade = cv2.CascadeClassifier(os.path.join('haarcascade_xml','haarcascade_frontalface_alt.xml'))

    gray = cv2.cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    b_boxes = face_cascade.detectMultiScale(
        gray,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(30, 30),
    )

    #Aumenta le dimensioni del bb del 10%
    new_b_boxes = []
    for box in b_boxes:
        new_b_box = []
        new_b_box.append(int(box[0] * 0.95) + 1)
        new_b_box.append(int(box[1] * 0.95) + 1)
        new_b_box.append(int(box[2] * 1.05) + 1)
        new_b_box.append(int(box[3] * 1.05) + 1)
        new_b_box = tuple(new_b_box)
        new_b_boxes.append(new_b_box)

    return new_b_boxes





#metodo che individua i subject che sono rimasti in camera dai frame precedenti tramite la distanza tra i centri dei bounding box evitando il riassegnamento a ogni nuovo frame
def split_old_new_lost_faces_centerDist(current_f_b_boxes, previous_frame, toll):

    old_f_dict={}

    for new_b_box in current_f_b_boxes:
        new_center=np.array((new_b_box[0] + int(new_b_box[2]/2) , new_b_box[1] + int(new_b_box[3]/2)))
        old=False

        for frame in previous_frame:
            if np.any(frame) and not old:
                for b_box, sbj in frame.items():
                    center = np.array((b_box[0] + int(b_box[2]/2) , b_box[1] + int(b_box[3]/2)))
                    dist = np.linalg.norm(new_center - center)
                    if dist < toll:
                        old = True
                        if type(sbj) is Subject:
                            old_f_dict[tuple(new_b_box)] = sbj
                        else:
                            sbj += 1
                            old_f_dict[tuple(new_b_box)] = sbj
        if(not old):
            old_f_dict[tuple(new_b_box)] = 0


    return old_f_dict


#metodo che individua i subject che sono rimasti in camera dai frame precedenti tramite la percentuale di overlap dei bounding box evitando il riassegnamento a ogni nuovo frame
def split_old_new_lost_faces(current_f_b_boxes, previous_frame, toll,frame_shape):

    old_f_dict={}

    for new_b_box in current_f_b_boxes:
        map = np.zeros(frame_shape)
        map[new_b_box[1] : new_b_box[1] + int(new_b_box[3]), new_b_box[0] : new_b_box[0] + new_b_box[2]] = True
        old=False

        for frame in previous_frame:
            if np.any(frame) and not old:
                for b_box, sbj in frame.items():
                    other_map = np.zeros(frame_shape)
                    other_map[b_box[1]: b_box[1] + int(b_box[3]), b_box[0]: b_box[0] + b_box[2]] = True

                    or_value = np.sum(np.logical_or(map,other_map))
                    and_value = np.sum(np.logical_and(map, other_map))

                    overlap = 1- (or_value - and_value)/or_value

                    if overlap > toll:
                        old = True
                        if type(sbj) is Subject:
                            old_f_dict[tuple(new_b_box)] = sbj
                        else:
                            sbj += 1
                            old_f_dict[tuple(new_b_box)] = sbj
        if(not old):
            old_f_dict[tuple(new_b_box)] = 0
    return old_f_dict




# Attraverso un meccanismo basato sulla distanza tra il nuovo descrittore e i centroidi assegna il nuovo soggetto: viene
def assign_subject(face ,nic_subjects,vgg):


    face_descriptor = extract_features_no_pca(face, vgg)
    neighbors=[]
    for subject in nic_subjects:
        neighbors.append(subject.get_n_neighbor(face_descriptor))

    if not np.array(neighbors).any():
        best_subj = None

    else:
        index=np.argmax(neighbors)
        best_subj=nic_subjects[index]

    if best_subj:
        best_subj.update_descriptors(face_descriptor)

    return best_subj


def draw_rect_and_names(dict, frame):
    font = cv2.FONT_HERSHEY_SIMPLEX

    for (x,y,w,h), subject in dict.items():
        cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
        if type(subject) is not Subject:
            text=""
        else:
            text = "Subject N." + str(subject.id)
        text_size = cv2.getTextSize(text=text, fontFace=font, fontScale=1, thickness=1)
        if (y - text_size[0][1] - 5 > 0): #il testo non sborda in alto
            y_pos = y - 5
        else:
            y_pos = y + h + text_size[0][1] + 5

        x_pos=x


        cv2.putText(frame, text, (x_pos, y_pos), font, fontScale=0.6, color=(0, 255, 0), thickness=2)


