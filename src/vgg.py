
#https://github.com/mzaradzki/neuralnets/blob/master/vgg_faces_keras/vgg_faces_demo.ipynb

import matplotlib.pyplot as plt
from PIL import Image
import numpy as np
import math
import copy
import cv2
from keras.models import Sequential, Model
from keras.layers import Input, Dense, Flatten, Dropout, Activation, Lambda, Permute, Reshape
from keras.layers import Convolution2D, ZeroPadding2D, MaxPooling2D

import os
import urllib.request

from scipy.io import loadmat


from keras import backend as K
# WARNING : important for images and tensors dimensions ordering


def convblock(cdim, nb, bits=3):
    L = []

    for k in range(1, bits + 1):
        convname = 'conv' + str(nb) + '_' + str(k)
        # L.append( Convolution2D(cdim, 3, 3, border_mode='same', activation='relu', name=convname) ) # Keras 1
        L.append(Convolution2D(cdim, kernel_size=(3, 3), padding='same', activation='relu', name=convname))  # Keras 2

    L.append(MaxPooling2D((2, 2), strides=(2, 2)))

    return L


def vgg_face_blank():

    withDO = True  # no effect during evaluation but usefull for fine-tuning

    if True:
        mdl = Sequential()

        # First layer is a dummy-permutation = Identity to specify input shape
        mdl.add(Permute((1, 2, 3), input_shape=(224, 224, 3)))  # WARNING : 0 is the sample dim

        for l in convblock(64, 1, bits=2):
            mdl.add(l)

        for l in convblock(128, 2, bits=2):
            mdl.add(l)

        for l in convblock(256, 3, bits=3):
            mdl.add(l)

        for l in convblock(512, 4, bits=3):
            mdl.add(l)

        for l in convblock(512, 5, bits=3):
            mdl.add(l)

        # mdl.add( Convolution2D(4096, 7, 7, activation='relu', name='fc6') ) # Keras 1
        mdl.add(Convolution2D(4096, kernel_size=(7, 7), activation='relu', name='fc6'))  # Keras 2
        if withDO:
            mdl.add(Dropout(0.5))
        # mdl.add( Convolution2D(4096, 1, 1, activation='relu', name='fc7') ) # Keras 1
        mdl.add(Convolution2D(4096, kernel_size=(1, 1), activation='relu', name='fc7'))  # Keras 2
        if withDO:
            mdl.add(Dropout(0.5))
        # mdl.add( Convolution2D(2622, 1, 1, name='fc8') ) # Keras 1
        mdl.add(Convolution2D(2622, kernel_size=(1, 1), activation='relu', name='fc8'))  # Keras 2
        mdl.add(Flatten())
        mdl.add(Activation('softmax'))

        return mdl

    else:
        # See following link for a version based on Keras functional API :
        # gist.github.com/EncodeTS/6bbe8cb8bebad7a672f0d872561782d9
        raise ValueError('not implemented')


def weight_compare(kmodel,l):
    kerasnames = [lr.name for lr in kmodel.layers]

    # WARNING : important setting as 2 of the 4 axis have same size dimension
    #prmt = (3,2,0,1) # INFO : for 'th' setting of 'dim_ordering'
    prmt = (0,1,2,3) # INFO : for 'channels_last' setting of 'image_data_format'

    for i in range(l.shape[1]):
        matname = l[0,i][0,0].name[0]
        mattype = l[0,i][0,0].type[0]
        if matname in kerasnames:
            kindex = kerasnames.index(matname)
            print(matname, mattype)
            print(l[0,i][0,0].weights[0,0].transpose(prmt).shape, l[0,i][0,0].weights[0,1].shape)
            print(kmodel.layers[kindex].get_weights()[0].shape, kmodel.layers[kindex].get_weights()[1].shape)
            print('------------------------------------------')
        else:
            print('MISSING : ', matname, mattype)
            print('------------------------------------------')

def copy_mat_to_keras(kmodel,l):

    kerasnames = [lr.name for lr in kmodel.layers]

    # WARNING : important setting as 2 of the 4 axis have same size dimension
    #prmt = (3,2,0,1) # INFO : for 'th' setting of 'dim_ordering'
    prmt = (0,1,2,3) # INFO : for 'channels_last' setting of 'image_data_format'

    for i in range(l.shape[1]):
        matname = l[0,i][0,0].name[0]
        if matname in kerasnames:
            kindex = kerasnames.index(matname)
            #print matname
            l_weights = l[0,i][0,0].weights[0,0]
            l_bias = l[0,i][0,0].weights[0,1]
            f_l_weights = l_weights.transpose(prmt)
            #f_l_weights = np.flip(f_l_weights, 2) # INFO : for 'th' setting in dim_ordering
            #f_l_weights = np.flip(f_l_weights, 3) # INFO : for 'th' setting in dim_ordering
            assert (f_l_weights.shape == kmodel.layers[kindex].get_weights()[0].shape)
            assert (l_bias.shape[1] == 1)
            assert (l_bias[:,0].shape == kmodel.layers[kindex].get_weights()[1].shape)
            assert (len(kmodel.layers[kindex].get_weights()) == 2)
            kmodel.layers[kindex].set_weights([f_l_weights, l_bias[:,0]])
            #print '------------------------------------------'


def pred(kmodel, crpimg, transform=False):
    # transform=True seems more robust but I think the RGB channels are not in right order

    imarr = np.array(crpimg).astype(np.float32)

    if transform:
        imarr[:, :, 0] -= 129.1863
        imarr[:, :, 1] -= 104.7624
        imarr[:, :, 2] -= 93.5940
        #
        # WARNING : in this script (https://github.com/rcmalli/keras-vggface) colours are switched
        aux = copy.copy(imarr)
        # imarr[:, :, 0] = aux[:, :, 2]
        # imarr[:, :, 2] = aux[:, :, 0]

        # imarr[:,:,0] -= 129.1863
        # imarr[:,:,1] -= 104.7624
        # imarr[:,:,2] -= 93.5940

    # imarr = imarr.transpose((2,0,1)) # INFO : for 'th' setting of 'dim_ordering'
    imarr = np.expand_dims(imarr, axis=0)

    out = kmodel.predict(imarr)

    best_index = np.argmax(out, axis=1)[0]
    best_name = description[best_index, 0]
    print(best_index, best_name[0], out[0, best_index], [np.min(out), np.max(out)])


def features(featmodel, crpimg, transform=False):
    # transform=True seems more robust but I think the RGB channels are not in right order

    imarr = np.array(crpimg).astype(np.float32)

    if transform:
        imarr[:, :, 0] -= 129.1863
        imarr[:, :, 1] -= 104.7624
        imarr[:, :, 2] -= 93.5940
        #
        # WARNING : in this script (https://github.com/rcmalli/keras-vggface) colours are switched
        aux = copy.copy(imarr)
        # imarr[:, :, 0] = aux[:, :, 2]
        # imarr[:, :, 2] = aux[:, :, 0]

        # imarr[:,:,0] -= 129.1863
        # imarr[:,:,1] -= 104.7624
        # imarr[:,:,2] -= 93.5940

    # imarr = imarr.transpose((2,0,1))
    imarr = np.expand_dims(imarr, axis=0)

    fvec = featmodel.predict(imarr)[0, :]
    # normalize
    normfvec = math.sqrt(fvec.dot(fvec))
    return fvec / normfvec



def load_vgg():
    K.set_image_data_format('channels_last')
    facemodel = vgg_face_blank()

    if not os.path.isfile('vgg-face.mat'):
        print('Downloading vgg-face weights...')
        url = 'http://www.vlfeat.org/matconvnet/models/vgg-face.mat'
        urllib.request.urlretrieve(url, 'vgg-face.mat')
        print('Download completed')

    data = loadmat('vgg-face.mat', matlab_compatible=False, struct_as_record=False)
    l = data['layers']
    #weight_compare(facemodel, l)
    copy_mat_to_keras(facemodel, l)
    featuremodel = Model(inputs=facemodel.layers[0].input, outputs=facemodel.layers[-2].output)
    return featuremodel




def extract_features(face, featuremodel,pca):
    resized = cv2.resize(face, (224, 224))
    f = features(featuremodel, resized, transform=False)
    f=pca.transform(f.reshape(1, -1))
    return f[0]

def extract_features_no_pca(face, featuremodel, to_RGB=True):
    resized = cv2.resize(face, (224, 224))
    if to_RGB:
        new_indexing = np.argsort([2, 1, 0])
        resized=resized[:,:,new_indexing]
    f = features(featuremodel, resized, transform=False)
    return f


if __name__=="__main__":
    #FIXME non testato!!!
    K.set_image_data_format('channels_last')
    facemodel = vgg_face_blank()
    facemodel.summary()
    #todo prima scaricare con 'wget http://www.vlfeat.org/matconvnet/models/vgg-face.mat'
    data = loadmat('vgg-face.mat', matlab_compatible=False, struct_as_record=False)
    l = data['layers']
    description = data['meta'][0, 0].classes[0, 0].description
    weight_compare(facemodel,l)
    copy_mat_to_keras(facemodel,l)
    featuremodel = Model(inputs=facemodel.layers[0].input, outputs=facemodel.layers[-2].output)
    img = cv2.imread('image_color.jpg')
    resized=cv2.resize(img, (224,224))
    f = features(featuremodel, resized, transform=True)
    # res= np.argmax(f)
    # print (res, np.max(f))
    # f.shape, f.dot(f)
