
from src.utilities import *
from src.vgg import load_vgg, extract_features_no_pca


FRAME_MEM = 8
TOLL = None
PERC = 0.70
IGN_TH = 5
ID_TH = 3

def live_reidentification(path_mahala= None, path_video = 0,threshold = 42):

    known_subjects = []

    #Carica la rete neurale
    vgg = load_vgg()

    #Carica la matrice di Mahalanobis utilizzata per calcolare (x-y)'M(x-y)
    if path_mahala:
        M = np.load(path_mahala)
    else:
        M = np.identity(vgg.layers[-2].output_shape[3])

    #La lista che mantiene i soggetti comparsi negli ultimi FRAME_MEM frames
    previous_frame = [None] * FRAME_MEM

    if not path_video:
        path_video = 0

    cap = cv2.VideoCapture(path_video)

    ret,frame=cap.read()
    #TOLL=int(0.1*min(frame.shape[:-1]))
    ret = True
    while(True):

        ret, frame = cap.read()
        if cv2.waitKey(1) & 0xFF == ord('q') or not ret:
            break

        b_boxes = extract_faces(frame)

        # dizionario con key = coordinate bounding box e value = subject
        #current_f_dict = split_old_new_lost_faces_centerDist(b_boxes, previous_frame, TOLL)
        current_f_dict = split_old_new_lost_faces(b_boxes, previous_frame, PERC, frame.shape[:-1])


        # nic=not in cam
        nic_subjects = list(set(known_subjects) - set(list(current_f_dict.values())))

        for box, subject in current_f_dict.items():

            face = frame[box[1]:box[1] + box[3], box[0]:box[0] + box[2]]

            # ghost faces management, una faccia viene presa in considerazione sse è presente per IGN_TH frames, si ignorano quindi ghost faces e facce mosse
            if type(subject) is not Subject:
                if subject in range(IGN_TH):
                    continue

                # Prima di identificare come nuovo soggetto la faccia entrata nel campo della telecamera si aspetta ID_TH frame
                if subject in range(IGN_TH, IGN_TH + ID_TH) :
                    candidate= assign_subject(face , nic_subjects,vgg)

                    if not candidate:
                        current_f_dict[tuple(box)] += 1

                    else:
                        subject = candidate
                        current_f_dict[tuple(box)] = subject
                        subject.update_descriptors(extract_features_no_pca(face, vgg))

                else:
                    subject=Subject(M.copy(),threshold)
                    current_f_dict[tuple(box)] = subject
                    known_subjects.append(subject)
                    subject.update_descriptors(extract_features_no_pca(face, vgg))

            #se ad un soggetto è già stato eseguito il clustering almeno una volta si estrae i suoi descrittori solo con probabilità 0.3
            elif(not subject.clusterized or np.random.rand(1)> 0.7):
                face_descriptor = extract_features_no_pca(face,vgg)
                subject.update_descriptors(face_descriptor)


        previous_frame.insert(0,current_f_dict)
        previous_frame.pop()

        draw_rect_and_names(current_f_dict, frame)
        cv2.imshow("frame", frame)

    cap.release()
    cv2.destroyAllWindows()


if __name__ == '__main__':
    live_reidentification()