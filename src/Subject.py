import numpy as np
from sklearn.cluster import MeanShift
from scipy.spatial.distance import pdist



MAX_FEATURES=25

class Subject:

    SUBJ_ID = 0
    ms = MeanShift(n_jobs=-2)

    def __init__(self,M , threshold):
        self.M=M
        self.id = Subject.SUBJ_ID
        Subject.SUBJ_ID += 1
        self.max_features = MAX_FEATURES
        self.descriptors=[]
        self.clusterized = False
        self.centroids=[]
        self.threshold = threshold

    def update_descriptors(self, face_descriptor): #aggiunge il descrittore se c'è posto altrimenti clustering

        if(len(self.descriptors) < self.max_features):
            self.descriptors.append(face_descriptor)
        else:
            self.clustering_on_descriptors()


    def clustering_on_descriptors(self):
        descriptors_array=np.array(self.descriptors)
        mean_shift=Subject.ms.fit(descriptors_array)
        centroids=list((mean_shift).cluster_centers_)
        self.centroids=centroids.copy()
        self.descriptors=centroids.copy()
        self.clusterized=True


    def get_n_neighbor(self, face_descriptor):

        n_neighbor=0

        for descriptor in self.centroids:
            dist = pdist(np.stack((descriptor, face_descriptor)),'mahalanobis', VI=self.M)
            if(dist[0]<= self.threshold):
                n_neighbor += 1

        return n_neighbor





