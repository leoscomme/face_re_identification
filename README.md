# Live Face Re-Identification

This repository performs runtime face re-identification on recorded videos or through the webcam stream.

### Prerequisites

To run this project is sufficient to have:

* Python 3.6
* [Anaconda 3](https://www.anaconda.com/download/#linux)
* [OpenCV](https://opencv.org/releases.html) 3.x or later
* [Keras API](https://keras.io/) with [TensorFlow](https://www.tensorflow.org/) as backend engine.

To replicate the Mahalanobis matrix and threshold computation ( pre-computed values are provided):

* [Metric-learn](https://github.com/metric-learn/metric-learn): WARNING: clone it from github and follow the instruction (do not use pip install)
* [FERET Database](https://www.nist.gov/itl/iad/image-group/color-feret-database)


## Getting Started

Before launching the face re-identification script, the application requires a Mahalanobis matrix and a threshold for dintinguish between different Subject descriptors.
To address this tasks we use a face database called FERET Database.

The scripts to calculate the Mahalanobis matrix training and the threshold are listed below:

* __launch_dataset_generation.py__: need the parameters "dataset_path", the path of the database directory and "target_dir", the path where the results will be stored. The output consists in a X.npy and a Y.npy file resp. the features file and the labels file
* __create_mahalanobis.py__: need the parameters  "path_data","path_label" (the output of the previous script) and a "target_dir" where the Mahalnobis matrix will be stored
* __launch_th_estimation.py__: need the parameters  "path_data","path_label" ( the same above) and a param called "path_mahalanobis" (output of the previous script), the script calculate a threshold based on the mean distance between subjects

##### Note: the process described above is not mandatory, in fact the Mahalanobis matrix and a valid threshold are provided in the repo.

## Running the application

The following script launches the live face re-identification:

* __launch_application.py__: needs  the parameter "--threshold" i.e. the threshold computed in the process above, if not given there is a default value,  "--path_mahala" the path of mahalanobis matrix, if not given the application use the identity matrix, "--path_video" the path to a video for face re-identification, if not given the application uses the webcam

*Warning*: Tha application uses Vgg-face Neural Network so it needs the related weights to work. 
Download at this [link](http://www.vlfeat.org/matconvnet/models/vgg-face.mat) and place them in the project directory otherwise, at the first run, the application will download them automatically (Note: SIZE: 1,1 GB!!)

In this repository is present:
* a mahalanobis matrix stored as .npy file (M_feret_300.npy)
* a directory with few videos (videos)

## Authors

* **Leonardo Scommegna** 
* **Cosimo Rulli**