import argparse
from src.metric_learner import learn_metric

def main():

    parser=argparse.ArgumentParser()

    parser.add_argument(
        "path_data",
        help="path to features file"
    )

    parser.add_argument(
        "path_label",
        help="path to labels file"
    )

    parser.add_argument(
        "target_dir",
        help="directory where dataset will be stored"
    )

    args=parser.parse_args()


    learn_metric(args.path_data, args.path_label, args.target_dir)



if __name__ == "__main__":
    main()