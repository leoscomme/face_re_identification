import argparse
from src.feret_dataset import generate_dataset


def main():

    parser=argparse.ArgumentParser()

    parser.add_argument(
        "dataset_path",
        help="path to face dataset"
    )

    parser.add_argument(
        "target_dir",
        help="directory where dataset will be stored"
    )



    args=parser.parse_args()


    generate_dataset(args.dataset_path, args.target_dir)



if __name__ == "__main__":
    main()