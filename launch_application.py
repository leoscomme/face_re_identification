import argparse
from src.face_re_identification import live_reidentification


def main():

    parser=argparse.ArgumentParser()

    parser.add_argument(
        "--path_mahala",
        help="path to Mahalanobis matrix file"
    )

    parser.add_argument(
        "--path_video",
        default = 0,
        help="path to video (if no video is given webcam is default) "
    )

    parser.add_argument(
        "--threshold",
        default= 42,
        help=" threshold for face verification (default is 42) "
    )


    args=parser.parse_args()


    live_reidentification(path_mahala=args.path_mahala, path_video= args.path_video,threshold=args.threshold)


if __name__ == "__main__":
    main()